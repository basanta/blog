<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FiscalYear extends Model
{
    //
    protected $fillable = ['fiscalyear','is_current','fromdate','todate','fromdatenp','todatenp'];

    protected  $table = 'fiscalyears';
}
