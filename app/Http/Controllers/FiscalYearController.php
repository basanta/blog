<?php

namespace App\Http\Controllers;

use App\FiscalYear;
use Illuminate\Http\Request;

class FiscalYearController extends Controller
{
    //
    public  function create(){
        return view('fiscalyear.create');
    }

    public  function  store(Request $request){

        FiscalYear::create([
                             'fiscalyear' => $request->input('fiscalyear'),
                             'is_current' => $request->input('is_current'),
                             'fromdate' => $request->input('fromdate'),
                             'todate' => $request->input('todate'),
                             'fromdatenp' => $request->input('fromdatenp'),
                             'todatenp' => $request->input('todatenp')
                         ]);
        return redirect()->route('fiscalyear.index');
    }

    function  index(){
//        $fiscalyears = Category::all();
//        $fiscalyears = Category::where('status',1)->orderby('created_at','desc')->get();
        $fiscalyears = FiscalYear::paginate(3);

        return view('fiscalyear.index',compact('fiscalyears'));
    }

    function  destroy($id){
        $fiscalyear = FiscalYear::find($id);
        $fiscalyear->delete();
        return redirect()->route('fiscalyear.index');
    }

    function  edit($id){
        $fiscalyear = FiscalYear::find($id);
        return view('fiscalyear.edit',compact('fiscalyear'));
    }

    function  update(Request $request,$id){
        $fiscalyear = FiscalYear::find($id);
        $fiscalyear->fiscalyear = $request->input('fiscalyear');
        $fiscalyear->is_current = $request->input('is_current');
        $fiscalyear->fromdate = $request->input('fromdate');
        $fiscalyear->todate = $request->input('todate');
        $fiscalyear->fromdatenp = $request->input('fromdatenp');
        $fiscalyear->todatenp = $request->input('todatenp');
        $fiscalyear->update();
        return redirect()->route('fiscalyear.index');
    }

    function  show($id){
        $fiscalyear = FiscalYear::find($id);
        return view('fiscalyear.show',compact('fiscalyear'));

    }
}
