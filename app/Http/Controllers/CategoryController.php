<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public  function create(){
        return view('category.create');
    }

    public  function  store(Request $request){

        Category::create([
            'name' => $request->input('name'),
            'rank' => $request->input('rank'),
            'status' => $request->input('status')
        ]);
        return redirect()->route('category.index');
    }

    function  index(){
//        $categories = Category::all();
//        $categories = Category::where('status',1)->orderby('created_at','desc')->get();
          $categories = Category::paginate(3);

        return view('category.index',compact('categories'));
    }

    function  destroy($id){
        $category = Category::find($id);
        $category->delete();
        return redirect()->route('category.index');
    }

    function  edit($id){
        $category = Category::find($id);
        return view('category.edit',compact('category'));
    }

    function  update(Request $request,$id){
        $category = Category::find($id);
        $category->name = $request->input('name');
        $category->rank = $request->input('rank');
        $category->status = $request->input('status');
        $category->update();
        return redirect()->route('category.index');
    }

    function  show($id){
        $category = Category::find($id);
        return view('category.show',compact('category'));

    }
}
