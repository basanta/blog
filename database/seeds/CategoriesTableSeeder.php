<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Category::create([
           'name' => 'Health',
           'rank' => 2,
           'status' => 1
        ]);
        \App\Category::create([
            'name' => 'Education',
            'rank' => 21,
            'status' => 1
        ]);
        \App\Category::create([
            'name' => 'Sports',
            'rank' => 12,
            'status' => 1
        ]);
    }
}
