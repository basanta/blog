<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFiscalyearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fiscalyears', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fiscalyear',50)->unique();
            $table->boolean('is_current')->default(0);
            $table->date('fromdate');
            $table->date('todate');
            $table->string('fromdatenp',15);
            $table->string('todatenp',15);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fiscalyears');
    }
}
