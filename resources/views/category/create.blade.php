@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Create Category</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                            <form action="{{route('category.store')}}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" name="name" id="name" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="rank">Rank</label>
                                    <input type="number" name="rank" id="rank" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <input type="radio" name="status" id="status" value="1">Active
                                    <input type="radio" name="status" id="status" value="0">De Active

                                </div>

                                <div class="form-group">

                                    <input type="submit" class="btn btn-success" value="Save Category" >
                                </div>

                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('title')
    This is Category Create
@endsection
