@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Edit Category</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                            <form action="{{route('category.update',$category->id)}}" method="post">
                                @csrf
                                <input type="hidden" name="_method" value="PUT">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" name="name" id="name" class="form-control" value="{{$category->name}}">
                                </div>
                                <div class="form-group">
                                    <label for="rank">Rank</label>
                                    <input type="number" name="rank" id="rank" class="form-control" value="{{$category->rank}}">
                                </div>
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    @if($category->status == 1)
                                        <input type="radio" name="status" id="status" value="1" checked>Active
                                        <input type="radio" name="status" id="status" value="0">De Active
                                    @else
                                        <input type="radio" name="status" id="status" value="1">Active
                                        <input type="radio" name="status" id="status" value="0" checked>De Active
                                    @endif

                                </div>

                                <div class="form-group">

                                    <input type="submit" class="btn btn-success" value="Update Category" >
                                </div>

                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('title')
    This is Category Create
@endsection
