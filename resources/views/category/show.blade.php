@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">View Details Category</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                           <table class="table table-bordered">
                               <tbody>
                                    <tr>
                                        <th>Name</th>
                                        <td>{{$category->name}}</td>
                                    </tr>
                                    <tr>
                                        <th>Rank</th>
                                        <td>{{$category->rank}}</td>
                                    </tr> <tr>
                                        <th>Status</th>
                                        <td>{{$category->status}}</td>
                                    </tr> <tr>
                                        <th>Created Date</th>
                                        <td>{{$category->created_at}}</td>
                                    </tr>
                                    <tr>
                                        <th>Updated Date</th>
                                        <td>{{$category->updated_at}}</td>
                                    </tr>
                                    <tr>
                                            <td>

                                                <a href="{{route('category.edit',$category->id)}}" class="btn btn-warning">Edit</a>

                                            </td>
                                    <td>
                                        <form onsubmit="return confirm('are you sure to delete?')" action="{{route('category.destroy',$category->id)}}" method="post">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button  class="btn btn-danger">Delete</button>
                                        </form>

                                    </td>
                                        </tr>

                               </tbody>
                           </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('title')
    This is Category List
@endsection
