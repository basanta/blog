@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">List Category</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                           <table class="table table-bordered">
                               <thead>
                                    <tr>
                                        <th>SN</th>
                                        <th>Name</th>
                                        <th>Rank</th>
                                        <th>Status</th>
                                        <th>Created Date</th>
                                        <th>Updated Date</th>
                                        <th>Action</th>
                                    </tr>
                               </thead>
                               <tbody>
                               @php($i=1)
                                    @foreach($categories as $category)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{$category->name}}</td>
                                            <td>{{$category->rank}}</td>
                                            <td>
                                               @if($category->status == 1)
                                                <span class="bg bg-success">Active</span>
                                                @else
                                                    <span class="bg bg-danger">De Active</span>
                                                @endif
                                            </td>
                                            <td>{{$category->created_at}}</td>
                                            <td>{{$category->updated_at}}</td>
                                            <td>
                                                <a href="{{route('category.show',$category->id)}}" class="btn btn-success">View</a>

                                                <a href="{{route('category.edit',$category->id)}}" class="btn btn-warning">Edit</a>
                                                <form onsubmit="return confirm('are you sure to delete?')" action="{{route('category.destroy',$category->id)}}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <button  class="btn btn-danger">Delete</button>
                                                </form>

                                            </td>
                                        </tr>
                                    @endforeach
                               </tbody>
                           </table>
                        {{$categories->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('title')
    This is Category List
@endsection
