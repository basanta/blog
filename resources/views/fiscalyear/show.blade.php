@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">View Details Category</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                           <table class="table table-bordered">
                               <tbody>
                                    <tr>
                                        <th>Fiscal Year</th>
                                        <td>{{$fiscalyear->fiscalyear}}</td>
                                    </tr>
                                    <tr>
                                        <th>Is Current Year</th>
                                        <td>@if($fiscalyear->is_current == 1)
                                                Yes
                                            @else
                                                No
                                            @endif </td>
                                    </tr> <tr>
                                        <th>From Date</th>
                                        <td>{{$fiscalyear->fromdate}}</td>
                                    </tr> <tr>
                                        <th>To Date</th>
                                        <td>{{$fiscalyear->todate}}</td>
                                    </tr><tr>
                                        <th>From Date BS</th>
                                        <td>{{$fiscalyear->fromdatenp}}</td>
                                    </tr> <tr>
                                        <th>To Date BS</th>
                                        <td>{{$fiscalyear->todatenp}}</td>
                                    </tr>
                                    
                                    <tr>
                                            <td>

                                                <a href="{{route('fiscalyear.edit',$fiscalyear->id)}}" class="btn btn-warning">Edit</a>

                                            </td>
                                    <td>
                                        <form onsubmit="return confirm('are you sure to delete?')" action="{{route('fiscalyear.destroy',$fiscalyear->id)}}" method="post">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button  class="btn btn-danger">Delete</button>
                                        </form>

                                    </td>
                                        </tr>

                               </tbody>
                           </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('title')
    This is Fiscal year
@endsection
