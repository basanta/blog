@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Create Fiscal Year</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                            <form action="{{route('fiscalyear.store')}}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label for="fiscalyear">Fiscal Year</label>
                                    <input type="text" name="fiscalyear" id="fiscalyear" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="is_current">Is Current Year</label><br>
                                    <input type="radio" name="is_current" id="is_current" value="1">Yes
                                    <input type="radio" name="is_current" id="is_current" value="0">No
                                </div>
                                <div class="form-group">
                                    <label for="fromdate">From Date AD</label>
                                    <input type="date" name="fromdate" id="fromdate" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="todate">To Date AD</label>
                                    <input type="date" name="todate" id="todate" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="fromdatebs">From Date BS</label>
                                    <input type="text" name="fromdatenp" id="fromdatenp" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="todatebs">To Date BS</label>
                                    <input type="text" name="todatenp" id="todatenp" class="form-control">
                                </div>

                                <div class="form-group">

                                    <input type="submit" class="btn btn-success" value="Save Fiscal Year" >
                                </div>

                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('title')
    This is Category Create
@endsection
