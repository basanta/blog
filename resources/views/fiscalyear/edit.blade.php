@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Edit Category</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                            
                            <form action="{{route('fiscalyear.update',$fiscalyear->id)}}" method="post">
                                @csrf
                                <input type="hidden" name="_method" value="PUT">
                                <div class="form-group">
                                    <label for="fiscalyear">Fiscal Year</label>
                                    <input type="text" name="fiscalyear" id="fiscalyear" class="form-control" value="{{$fiscalyear->fiscalyear}}">
                                </div>
                                <div class="form-group">
                                    <label for="is_current">Is Current Year</label><br>
                                    @if($fiscalyear->is_current == 1)
                                        <input type="radio" name="is_current" id="is_current" value="1" checked>Yes
                                        <input type="radio" name="is_current" id="is_current" value="0">No
                                    @else
                                        <input type="radio" name="is_current" id="is_current" value="1">Yes
                                        <input type="radio" name="is_current" id="is_current" value="0" checked>No
                                    @endif
                                    
                                </div>
                                <div class="form-group">
                                    <label for="fromdate">From Date AD</label>
                                    <input type="date" name="fromdate" id="fromdate" class="form-control" value="{{$fiscalyear->fromdate}}">
                                </div>
                                <div class="form-group">
                                    <label for="todate">To Date AD</label>
                                    <input type="date" name="todate" id="todate" class="form-control" value="{{$fiscalyear->todate}}">
                                </div>
                                <div class="form-group">
                                    <label for="fromdatebs">From Date BS</label>
                                    <input type="text" name="fromdatenp" id="fromdatenp" class="form-control" value="{{$fiscalyear->fromdatenp}}">
                                </div>
                                <div class="form-group">
                                    <label for="todatebs">To Date BS</label>
                                    <input type="text" name="todatenp" id="todatenp" class="form-control"  value="{{$fiscalyear->todatenp}}">
                                </div>
    
                                <div class="form-group">
        
                                    <input type="submit" class="btn btn-success" value="Update Fiscal Year" >
                                </div>
                                

                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('title')
    This is Category Create
@endsection
