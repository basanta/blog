@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">List Fiscal Year</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                           <table class="table table-bordered">
                               <thead>
                                    <tr>
                                        <th>SN</th>
                                        <th>Fiscal Year</th>
                                        <th>Is Current</th>
                                        <th>From Date</th>
                                        <th>To Date</th>
                                        <th>From Date BS</th>
                                        <th>To Date BS</th>
                                        <th>Action</th>
                                    </tr>
                               </thead>
                               <tbody>
                               @php($i=1)
                                    @foreach($fiscalyears as $fy)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{$fy->fiscalyear}}</td>
                                            <td> @if($fy->is_current == 1)
                                                    Yes
                                                 @else
                                                    No
                                                @endif
                                            
                                            </td>
                                            <td>{{$fy->fromdate}}</td>
                                            <td>{{$fy->todate}}</td>
                                            <td>{{$fy->fromdatenp}}</td>
                                            <td>{{$fy->todatenp}}</td>
                                            <td>
                                                <a href="{{route('fiscalyear.show',$fy->id)}}" class="btn btn-success">View</a>

                                                <a href="{{route('fiscalyear.edit',$fy->id)}}" class="btn btn-warning">Edit</a>
                                                <form onsubmit="return confirm('are you sure to delete?')" action="{{route('fiscalyear.destroy',$fy->id)}}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <button  class="btn btn-danger">Delete</button>
                                                </form>

                                            </td>
                                        </tr>
                                    @endforeach
                               </tbody>
                           </table>
                        {{$fiscalyears->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('title')
    This is Category List
@endsection
