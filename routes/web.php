<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	// return "test";
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/category/create', 'CategoryController@create')->name('category.create');

Route::post('/category', 'CategoryController@store')->name('category.store');

Route::get('/category', 'CategoryController@index')->name('category.index');

Route::delete('/category/{id}', 'CategoryController@destroy')->name('category.destroy');

Route::get('/category/{id}/edit', 'CategoryController@edit')->name('category.edit');

Route::put('/category/{id}', 'CategoryController@update')->name('category.update');

Route::get('/category/{id}', 'CategoryController@show')->name('category.show');


Route::get('/fiscalyear/create', 'FiscalYearController@create')->name('fiscalyear.create');

Route::post('/fiscalyear', 'FiscalYearController@store')->name('fiscalyear.store');

Route::get('/fiscalyear', 'FiscalYearController@index')->name('fiscalyear.index');

Route::delete('/fiscalyear/{id}', 'FiscalYearController@destroy')->name('fiscalyear.destroy');

Route::get('/fiscalyear/{id}/edit', 'FiscalYearController@edit')->name('fiscalyear.edit');

Route::put('/fiscalyear/{id}', 'FiscalYearController@update')->name('fiscalyear.update');

Route::get('/fiscalyear/{id}', 'FiscalYearController@show')->name('fiscalyear.show');